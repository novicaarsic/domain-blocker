import time
import sys 
from datetime import datetime as dt
from playsound import playsound
from plyer import notification

hosts_path = "/etc/hosts"
redirect = "127.0.0.1"
domain_path = "domains"
domain_list = []

# funkcija za postavljanje sati
def setHrs(hrs):
    return dt(dt.now().year, dt.now().month, dt.now().day, hrs)

# funkcija za notifikacije 
def notify(msg):
    notification.notify(
            title = "domain-blocker.py",
            message = msg,
            timeout = 10
            )
    playsound("IM.wav")

# dodavanje sajtova u hosts fajla
def addToHosts():
    with open(hosts_path, "r+") as file:
        content=file.read()
        for domain in domain_list:
            if domain in content:
                pass
            else:
                file.write(redirect + "\t " + domain + "\n")

# uklanjanje sajtova iz hosts fajla
def remFromHosts():
    with open(hosts_path, "r+") as file:
        content = file.readline()
        file.seek(0)
        for line in content:
            if not any(domain in line for domain in domain_list):
                file.write(line)
        file.truncate()

#=================================================================#
#=================================================================#
#=================================================================#

# provera da li se nalazimo na windows sistemu
if sys.platform == "win32":
    print("Windows system detecter")
    hosts_path = r"C:\Windows\System32\drivers\etc\hosts"

# provera da li je dovoljno argumenata uneto
if len(sys.argv) < 3:
    sys.exit("Usage: domain-blocker.py [0-23] [0-23]")

# čitanje iz domains fajla i ubacivanje u listu domena
for line in open(domain_path, "r"):
    if line.startswith("#") == False:
        domain_list.append(line[:-1])

# glavni loop

wrkStt = None
while True:
    try:
        if setHrs(int(sys.argv[1])) < dt.now() < setHrs(int(sys.argv[2])):
            print("Working hours...")
            addToHosts()

            if not wrkStt:
                wrkStt = True
                notify("Working hours")

        else:
            print("Fun hours...")
            remFromHosts()
        
            if wrkStt:
                wrkStt = False
                notify("Working hours no more!")

        time.sleep(5)
    except:
        print("Only values between 0 and 23 are allowed.")
        exit()
