# domain-blocker.py
Skripta koja blokira sajtove u određenom vremenskom periodu.

## Kako koristiti
**Potrebne su administratorke privilegije za pokretanje skripte**
1. Korisnik prvo mora ubaciti domene u 'domains' fajl koje želi blokirati
2. Korisnik pri pokretanju skripte daje dva argumenta, broj između 0 i 23
    Primer: `python3 domain-blocker.py 7 16`

## Instalacija
1. Fork/Clone/Download ovaj repo
    `git clone https://novicaarsic@bitbucket.org/novicaarsic/domain-blocker.git`
2. Ulazimo u direktorijum repo-a
    `cd domain-blocker`
3. Koristi `pip install -r requirements.txt` da instalira potrebne py module

Skripta se može sada pokrenuti